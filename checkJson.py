from argparse import ArgumentParser  # parser with doc
import os
import json

__VERSION__ = '0.3'


def get_path(item, path):
    """
    Traverses a nested structure of dictionaries/lists using a given path and returns the value at the specified path.

    Args:
        item (dict or list): The nested structure to traverse.
        path (list): A list of keys or indices representing the path to follow within the nested structure.

    Returns:
        Any: The value at the specified path within the nested structure.

    Example:
        item = {'a': {'b': [1, 2, 3]}}
        path = ['a', 'b', 1]
        value = get_path(item, path)
        # Returns 2
    """
    try:
        for i in range(len(path)):
            item = item[path[i]]
    except NameError:
        print('ERROR: {} in in {} ?'.format(path, item))
        exit(1)
    return item


def extract_data(field, data):
    """
      Extracts specific data fields from a nested dictionary list and returns a nested list of extracted values paired
      with their corresponding 'id' values.

      :param field: A list of fields to extract from the data dictionary. Each field is represented by a tuple with two
                    elements: the first element is the parent key, and the second element is the child key. If the child
                    key is not nested within the parent key, a single string can be used instead of a tuple.
      :type field: List

      :param data: A list of dictionaries containing the data.
      :type data: List

      :return: A nested list of extracted values paired with their corresponding 'id' values.
      :rtype: List
      """
    return \
        [
            [
                (
                    get_path(item, field[x])
                    if len(field[x][0]) > 1
                    else item[field[x]],
                    item['id']
                )
                for item in data
            ]
            for x in range(len(field))
        ]


def check_duplicates(data, verbose: bool, all_checks: bool) -> int:
    """
    Check if there is the same value for different Vegetables
    :param data: The JSON file.
    :param verbose: Show details in the console if true.
    :param all_checks: If true, doesn't stop the program in case of error.
    :return:  Status,   0:ok, 1:error(s)
    """
    res = 0
    field_duplicates_check = [
        'name',
        'id'
    ]
    for duplicate_item in field_duplicates_check:
        values = [item[duplicate_item] for item in data]
        for value in values:
            if values.count(value) > 1:  # is a duplicate
                if verbose:
                    print('\t{} {} is duplicated ({} values)'.format(duplicate_item, value, values.count(value)))
                res = 1
                if not all_checks:
                    return res
    if verbose and res != 1:
        print('IDs check passed without error.')
    return res


def has_type(value, types):
    """
    Checks if a value matches any of the specified types.

    Args:
        value: The value to be checked.
        types: A list of types or values representing types to match against.

    Returns:
        bool: True if the value matches any of the specified types, False otherwise.

    Examples:
    >>> has_type(5, [int, float])
    True
    >>> has_type("Hello", [str, int, bool])
    True
    >>> has_type(True, [int, float])
    False
"""
    for type_c in types:
        if type(value) is type_c:
            return True
        if value == type_c:
            return True
    return False


def check_type(data, verbose: bool, all_checks: bool) -> int:
    """
    Check if the 'type' property match the type_allowed array
    :param data: The JSON file.
    :param verbose: Show details in the console if true.
    :param all_checks: If true, doesn't stop the program in case of error.
    :return: Error status,   0:ok, 1:error(s)
    """
    res = 0
    items_to_check = [
        ['id', [str]],
        ['name', [str]],
        ['type', [
            ["fruit", "herb", "vegetable", "plant"],
            ["leafy", "tuber/root", "bulb", "sprout", "stem", "seed", type(None)],
        ], {"associative": True}
         ],
        ['fromTree', [bool]],
        ['family', [str]],
        ['synopsis', [str]],
        ['preservation', [str, type(None)]],
        ['nutritional', [str, type(None)]],
        ['vitamin', [str, type(None)]],
        ['history', [str]],
        ['imagePath', [str]],
        [['cultureSheets', 'quickInfos', 'compostNeed'], [0, 1, 2, type(None)]],
        [['cultureSheets', 'quickInfos', 'waterNeeds'], [1, 2, 3, type(None)]],
        [['cultureSheets', 'quickInfos', 'coldResistance'], [-1, 1, 2, type(None)]],
        [['cultureSheets', 'quickInfos', 'rowSpacing'], [int, type(None)]],
        [['cultureSheets', 'quickInfos', 'spacingBtRows'], [int, type(None)]],
        [['cultureSheets', 'monthOfPlanting'], [int, list]],
        [['cultureSheets', 'monthOfHarvest'], [int, list]],
        [['cultureSheets', 'plantingInterval'], [int, type(None)]],
        [['cultureSheets', 'plantation'], [str]],
        [['cultureSheets', 'plantation'], [str, type(None)]],
        [['cultureSheets', 'soil'], [str, type(None)]],
        [['cultureSheets', 'tasks'], [str, type(None)]],
        [['cultureSheets', 'favourableAssociation'], [str, type(None)]],
        [['cultureSheets', 'unfavourableAssociation'], [str, type(None)]],
        [['cultureSheets', 'favourablePrecedents'], [str, type(None)]],
        [['cultureSheets', 'unfavourablePrecedents'], [str, type(None)]],
    ]
    for key, types_allowed, *options in items_to_check:
        options = dict(options[0]) if options else None
        for value, vegetable_id in extract_data([key], data)[0]:
            item_is_valid = False
            if type(value) is list:
                for i in range(len(value)):
                    types_c = types_allowed[i] if options and options.get("associative") else types_allowed
                    item_is_valid = has_type(value[i], types_c)
                    if not item_is_valid:
                        break
            else:
                item_is_valid = has_type(value, types_allowed)
            if not item_is_valid:
                if verbose:
                    print('VegetableId: ', vegetable_id, ' field : ', key, ' is not on type: ',
                          types_allowed)
                    res = 1
                if not all_checks:
                    return res
    if verbose and res != 1:
        print('Types check passed without error.')
    return res


def check_names(data, verbose: bool, all_checks: bool) -> int:
    """
    Check that the names of the Vegetables begin with a capital letter
    :param data: The JSON file
    :param verbose: Show details in the console if true.
    :param all_checks: If true, doesn't stop the program in case of error.
    :return: Error status,   0:ok, 1:error(s)
    """
    res = 0
    field_uppercase_check = [
        'name',
        ['cultureSheets', 'favourableAssociation'],
        ['cultureSheets', 'unfavourableAssociation'],
        ['cultureSheets', 'favourablePrecedents'],
        ['cultureSheets', 'unfavourablePrecedents']
    ]
    # Make sure the first letter of vegetables is capitalized
    if all([True if 'cultureSheets' in item.keys() else False for item in
            data]):  # Check if 'cultureSheets' key exists.
        vegetablesNames = extract_data(field_uppercase_check, data)
        # Example of vegetablesNames: [(VegetableName, Vegetable ID)]
        # [[('Aubergine', 'aube')], [(['Cucumber', 'Potato'], 'aube')], [(None, 'aube')], [(['Tomato'], 'aube')]]
        vegetableNumber = len(vegetablesNames[0])
        for i in range(vegetableNumber):
            for y in range(len(field_uppercase_check)):
                if isinstance(vegetablesNames[y][i][0], list):  # If it's an array: ['Cucumber', 'Potato']
                    for z in vegetablesNames[y][i][0]:
                        if not z[0].isupper():
                            if verbose:
                                print(z, 'with ID :', vegetablesNames[y][i][1], 'is incorrect. Does the word start '
                                                                                'with a capital letter?')
                            res = 1
                            if not all_checks:
                                return res
                else:  # It's only one name: 'Aubergine'
                    if vegetablesNames[y][i][0] is not None and not vegetablesNames[y][i][0][0].isupper():
                        if verbose:
                            print(vegetablesNames[y][i][0], 'with ID :', vegetablesNames[y][i][1],
                                  ' is incorrect. Does the word start with a capital letter?')
                        res = 1
                        if not all_checks:
                            return res
    if verbose and res != 1:
        print('Capital letters check passed without error.')
    elif verbose:
        print(field_uppercase_check[0] + ' does\'t exist')
    return res


def check_arrays(data, verbose: bool, all_checks: bool) -> int:
    """
    If contain only one number, data should not be an array
    :param data: The JSON file.
    :param verbose: Show details in the console if true.
    :param all_checks: If true, doesn't stop the program in case of error.
    :return: Error status,   0:ok, 1:error(s)
    """
    res = 0
    field_array_check = [
        ['cultureSheets', 'monthOfPlanting'],
        ['cultureSheets', 'monthOfHarvest'],
    ]
    # Check if 'cultureSheets' key exists.
    if all([True if 'cultureSheets' in item.keys() else False for item in data]):
        arrays = extract_data(field_array_check, data)  # Field to check
        for item in range(len(arrays[0])):
            if isinstance(arrays[0][item][0], list):  # If is an array
                if len(arrays[0][item][0]) <= 1:
                    if verbose:
                        print(arrays[0][item][0], 'with ID :', arrays[0][item][1],
                              ' is incorrect. Does the array contain only one value ?')
                    res = 1
                    if not all_checks:
                        return res
    if verbose and res != 1:
        print('Array check passed without error.')
    return res


def check_image_path(data, verbose: bool, all_checks: bool) -> int:
    """
    Check if the image path matches the Vegetable ID.
    :param data: The json file.
    :param verbose: Show details in the console if true.
    :param all_checks: If true, doesn't stop the program in case of error.
    :return: Error status,   0:ok, 1:error(s)
    """
    response = 0
    name_png = [(item['imagePath'].split('/')[-1].replace(".png", "")) for item in data]
    name_list = [[item['id'], item['id']] for item in data]
    for i in range(len(name_list)):
        if name_list[i][0] not in name_png:
            response = 1
            if verbose:
                print('\n', name_list[i][0], ' error with ID ', name_list[i][1])
            if not all_checks:
                return response
    if verbose and response != 1:
        print('Image path check passed without error.')
    return response


def check_json(path: str, verbose: bool, all_checks: bool) -> int:
    """
    Check the JSON file created by jsonBuilder.sh
    Check id duplicate, type and Vegetables names
    :param path: The JSON path
    :param verbose: Show details in the console if true.
    :param all_checks: If true, doesn't stop the program in case of error.
    :return: Error status,   0:ok, 1:error(s)
    """
    response = 0
    if not os.path.exists(path):
        return 1

    try:
        with open(path, 'r') as file:
            data = json.load(file)
    except ValueError as err:
        print(path, " -> Can't open JSON, an error occurred")
        print(err)

    if verbose:
        print('CHECK FOR DUPLICATES...')
    res = check_duplicates(data, verbose, all_checks)
    if not all_checks and res == 1:
        return res
    elif res == 1:
        response = 1

    if verbose:
        print('\n\nCHECK VEGETABLES TYPES...')
    res = check_type(data, verbose, all_checks)
    if not all_checks and res == 1:
        return res
    elif res == 1:
        response = 1

    if verbose:
        print('\n\nCHECK VEGETABLES NAMES...')
    res = check_names(data, verbose, all_checks)
    if not all_checks and res == 1:
        return res
    elif res == 1:
        response = 1

    if verbose:
        print('\n\nCHECK VEGETABLES IMAGE PATH...')
    res = check_image_path(data, verbose, all_checks)
    if not all_checks and res == 1:
        return res
    elif res == 1:
        response = 1

    if verbose:
        print('\n\nCHECK ARRAYS...')
    res = check_arrays(data, verbose, all_checks)
    if not all_checks and res == 1:
        return res
    elif res == 1:
        response = 1

    return response


if __name__ == "__main__":
    parser = ArgumentParser(description="Checks the json at given path for saving in db")
    parser.add_argument("path", type=str, help="path of the json file to check")
    parser.add_argument('--all-checks', '-a', action='store_true', default=False,
                        help="if set, the script will always do all checks even if one failed before")
    parser.add_argument('--verbose', '-vb', action='store_true', default=False,
                        help="if set, the script will print out all actions and used data for checks for debug")
    parser.add_argument('--version', '-v', action='version', version=f'check json script v{__VERSION__}, by Raphaël')
    kwargs = vars(parser.parse_args())

    exit(check_json(**kwargs))
