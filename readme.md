# MyGardenDB

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/m9712/vegetables-db?branch=main&style=flat-square)

[MyGardenDB](https://m9712.gitlab.io/vegetables-db/) repo manages fruit/vegetables and the general MyGarden doc.

## Getting Started :truck:

- [Getting Started with development setup](https://m9712.gitlab.io/vegetables-db/Getting%20started/Development_Setup/)
- [Getting Started with translation](https://m9712.gitlab.io/vegetables-db/Getting%20started/How_to_translate_MyGarden/)
- [Getting Started with Vegetables](https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/)

## Get in touch :speech_balloon:
* [:material-discord: Discord](https://discord.gg/CTVrrCa2YQ)
* [:material-matrix: Element](https://matrix.to/#/#MyGarden:matrix.org)

## Licence
The app core is under GPLv3 but be careful the vegetables Images and vegetables data are limited are restricted to copyright.
