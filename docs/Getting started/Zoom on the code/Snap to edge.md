This section aims to explain how the snap to edge works in MyGarden.

The features can be found in the file: [src/app/tab2/garden-canvas/fabric.service.ts](https://gitlab.com/m9712/mygarden/-/blob/master/src/app/tab2/garden-canvas/fabric.service.ts)

## What is snap to edge ?
Snap to edge helps the user to nest elements together while maintaining perfect alignment.
![](../../../assets/images/snap_example.gif)

[Fabric.JS](http://fabricjs.com/) does not directly include snap to edge functionality so it needs to be developed.
The idea is therefore to assume the coordinates of each element and to check if an object is close to another one. If this is the case then the new coordinates are calculated according to the target object.

An example:
```ts
snapZone = 15;
if (activeObjectRight > targetObjLeft - snapZone &&
    activeObjectRight < targetObjLeft + snapZone) {
    activeObject.target.set({
        left: targetObjLeft - activeObjectWidth,
    }).setCoords();
}
```
`activeObjectRight`: Is the object currently moving by the user. <br>
`targetObjectRight`: Is the object tested.

If the coordinates of 'activeObjectRight' correspond to +- 'snapZone' of the coordinates of 'targetObjectLeft' then the coordinates of 'activeObject' are updated.
However, for aesthetic reasons, a "stroke" is added to each object. The new coordinates must be adjusted accordingly.
What you have to understand with the current configuration is that when an object is resized the size does not change but the scale. This complicates things slightly.

![](../../../assets/images/snap_scale_example.png)

As you can see in this image, the black borders are bigger on the right and left of the enlarged object.
It is possible in Fabric.JS to set the 'strokeUniform' option to true and thus have borders of the same size.
It is possible in Fabric.JS to set the 'strokeUniform' option to true and thus have borders of the same size however if we calculate the new size with (objectOriginalWidth * scaleX) we get the object size + the full border width. Indeed Fabric.JS sets the color of the border sruplus to 'transparent'. So we have an offset that we have to compensate.

## Understand stroke correction:

As mentioned before, we need to compensate for the distance created by the border.
To overcome this problem it is necessary to adjust our previous code.

```ts
snapZone = 15;
if (activeObjectRight > targetObjLeft - snapZone &&
    activeObjectRight < targetObjLeft + snapZone) {
    activeObject.target.set({
        left: targetObjLeft - activeObjectWidth + strokeCorrectionX(),
    }).setCoords();
}
```

```ts
function strokeCorrectionX() {
return 0.5 * (this.defaultStrokeWidth + this.defaultStrokeWidth * activeObjectScaleX);
};
```

The strokeCorrectionX function is used to calculate the length to compensate. Let's try to see how it works.

![](../../../assets/images/stroke_example.png)

As you can see on this image above, the snap to edge function works perfectly well if 'strokeUniform' is set to 'false' unfortunately if 'strokeUniform' is set to 'true' there is a small white area between the two objects.

![](../../../assets/images/stroke_explain.png)

The strokeCorrectionX function calculates the length of the white stripe plus half of 'defaultStrokeWidth' to make the edges of the two objects merge.

Here is the principle of operation of this function of course it is also necessary to take into account the size of the target object for, here in the example the scale of the target object was equal to 1 to not need to calculate a compensation.

Here is an example

```ts
function strokeCorrectionX () {
// Apply correction for the activeObject
let correction = 0.5 * (this.defaultStrokeWidth + this.defaultStrokeWidth * activeObjectScaleX);

// Apply correction for targetObject
correction = correction + 0.5 * (this.defaultStrokeWidth * targetObjectScaleX - this.defaultStrokeWidth);
return correction;
};
```
