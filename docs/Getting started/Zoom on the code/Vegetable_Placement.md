
The generation is based on GA (Genetic Algorithm), if you don't know what GA is, you could watch this short video.

This section is a brief description of how MyGarden works to generate gardens, I will not explain how GA works but how GA is implemented in MyGarden.

![Image title](https://i.ytimg.com/vi_webp/uQj5UNhCPuo/maxresdefault.webp?v=5f4de8f0){ width="300" }

[ Genetic Algorithms Explained By Example ](https://www.youtube.com/watch?v=uQj5UNhCPuo)

## Why Genetic Algorithm

The first version of MyGarden used a Greedy algorithm, it was fast but add constraints was very hard due to the
architecture of the algorithm (Constructive Algorithms) what's more it was not always (often) the best solution, a better alternative is to use Genetic Algorithm.
The genetic algorithm is based on the simulation of evolution described by Darwin in the middle of the 19th century.
It used natural selection to approximate the best solution, in the case of MyGarden, the best position for each
fruit/vegetable.

### Genetic Algorithm Advantages

- Easy to implement (almost 😀).
- Adding constraints is very simple compared to Greedy's algorithm.
- Quite fast compared to brute force.
- Works well.


### Genetic Algorithm Structure

#### 1. Genetic representation of a solution (Chromosome)
First, we need to generate a chromosome. In our case, a chromosome is just an array of numbers that represent vegetables (gene) and their position in the array (gene index).
We have to take into account, that the genetic algorithm is executed for each month and that it is essential to keep the genes of the vegetables present the previous month at the same place to avoid any inconsistencies.

!!! note ""

    For example with the following inputs, the output could be :
    
    **INPUTS : **
    
    **Vegetables :** ['Aubergine', 'Potao', 'Tomato']

    **Zones : ** ['A', 'B', 'C', 'D']

    **OUTPUTS : **

    **Function output : ** [1, 0, -1, 2] <- Chromosome

    * The gene **1** corresponding to the Potato (index in `vegetables` of Potato is 1) on the zone 'A'
    * The gene **2** corresponding to the Tomato (index in `vegetables` of Tomato is 2) on the zone 'D'
    * -1 mean that the zone is empty an contain any vegetable

#### 2. A function to generate new solutions (Population)

Population is simply a list of Chromosome.

#### 3. Fitness function

We need a fitness function to evaluate a solution.
For now MyGarden take into account the following check point:

- Favorable/Unfavorable associations between vegetables
- Rotation of vegetables over the years

???+ info

    **Planned check**

    * Previuous Favorable/Unfavorable association

    * Zones contacts

#### 4. Selection function

This function select a pair of chromosome which will be the parent of two new chromosome for the next generation.


#### 5. Crossover function

Crossover is a genetic operator used to combine the genetic information of two parents to generate new offspring.
In the case of MyGarden we have to keep in mind that old vegetables can't change of position.
A solution is to use [Order crossover (OX1)](https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm)#Uniform_Crossover_and_Half_Uniform_Crossover)
where the selected genes are those vegetables.


#### 6. Mutation function


_Mutation is a genetic operator used to maintain genetic diversity of the chromosomes of a population of a genetic or, more generally, an evolutionary algorithm (EA). It is analogous to biological mutation._
[Wikipedia](https://en.wikipedia.org/wiki/Mutation_(genetic_algorithm))

Like before, static vegetables can't be mutated.
