MyGarden uses Crowdin to translate the App.

We manage translations via [Crowdin](https://crowdin.com/project/mygarden-core-app).
So just request joining the translation team for MyGarden on the site and start translating.
All translations will then be automatically pushed to the [MyGarden repository](https://gitlab.com/m9712/mygarden), 
there is no need for any pull request for translations.
All you need is to create a [Crowding](https://crowdin.com/) account and join the [projet](https://crowdin.com/project/mygarden-core-app).

## 🗃️ Translation structure
!!! warning
    
    You don't have to edit these files directly, please go through Crowdin for that.

- `src/assets/i18n`: Translation files of the App core
- `src/assets/vegetables/*.json`: Translation files of vegetables.

## 🇪🇺 Data to translate 

For vegetables everything can't be translated (like the vegetable id), 
follow this [link](https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/) (section: **Needs to be translated ?**) for more details.
Normally, these strings have a tag `DO NOT TRANSLATE` and are invisible. But you need to be careful, because some strings have not been taken into account.


## 🌐 Supported language

 - 🇫🇷 French
 - 🇬🇧 English

If you want to translate MyGarden in another language, please open an [issue](https://gitlab.com/m9712/mygarden/-/issues) or contact me on [discord](https://discord.gg/XPNBsarq8t).
