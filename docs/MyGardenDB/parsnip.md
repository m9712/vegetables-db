[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/parsnip.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/parsnip_5415379?term=parsnip&page=1&position=16&page=1&position=16&related_id=5415379&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "parsnip",
  "name": "Parsnip",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Apiaceae",
  "synopsis": "It contains many minerals and vitamins. It can be eaten raw or cooked.",
  "preservation": "7 days in the crisper",
  "nutritional": "90 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin B9  ",
  "history": "The inhabitants of the Mediterranean basin began to consume parsnips long before our era. But Greeks and Romans did not seem to appreciate it, apparently because of its tough skin and fibrous core. It will be progressively domesticated and improved from the 6th century.",
  "imagePath": "../../../assets/vegetables/icons/parsnip.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 2,
      "rowSpacing": 15,
      "spacingBtRows": 30
    },
    "monthOfPlanting": [3, 6],
    "monthOfHarvest": [6, 9],
    "plantingInterval": null,
    "plantation": "Sow in rows from March to June. Thin out when plants have 2 or 3 leaves and leave 10 cm between each plant.",
    "harvest": "3 months after sowing.",
    "soil": "Fresh and light",
    "tasks": "Hoe, weed and water. Parsnips fear summer drought.",
    "favourableAssociation": ["Broccoli", "Cabbage", "Radish", "Tomato"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/panais) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 119

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
