[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/artichoke.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/artichoke_676448?term=artichoke&page=1&position=42&page=1&position=42&related_id=676448&origin=style)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "artichoke",
  "name": "Artichoke",
  "type": ["vegetable", "leafy"],
  "fromTree": false,
  "family": "Asteraceae",
  "synopsis": "This edible flower with its fine flavor garnishes tables in spring and summer. It's a perennial plant adapted to relatively mild winters. It is easy to grow in Brittany or around the Mediterranean.",
  "preservation": "A few days in the crisper",
  "nutritional": "47 kcal for 100g",
  "vitamin": "Rich in vitamin B9",
  "history": "Originally from the Mediterranean basin, the artichoke is the result of various botanical crossings. The first traces of this vegetable are found in Italy, in the middle of the Renaissance (middle of the 16th century). The prerogative of kings: The artichoke is introduced for the first time in France on the table of the queen Catherine de Médicis. The vegetable also became the favorite of Louis XIV, who liked it so much that there were 5 different species at Versailles during the Sun King's time: the White, the Green, the Violet, the Red and the Sweet of Genoa. It was not until 1810 that an agronomist from the Paris region developed the Camus de Bretagne, the favorite artichoke of the French. Did you know that? What is commonly called the heart or the bottom of the artichoke is the receptacle of the unopened flowers of the plant. These flowers form the hay, a kind of hair that covers the artichoke heart.",
  "imagePath": "../../../assets/vegetables/icons/artichoke.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 1,
      "coldResistance": -1,
      "rowSpacing": 100,
      "spacingBtRows": 100
    },
    "monthOfPlanting": [1, 3],
    "monthOfHarvest": [6,9],
    "plantingInterval": null,
    "plantation": "The artichoke is a perennial plant that requires little maintenance. You just have to take care to protect it from freezing in winter, by staking it at its base and mulching it copiously.",
    "harvest": "Cut the inflorescences before they open, using pruning shears. Early harvests encourage new inflorescences to appear.",
    "soil": null,
    "tasks": "Let 4 or 5 artichokes bloom for the beauty of the flowers and to attract many beneficial insects.",
    "favourableAssociation": ["Endive", "Lettuce"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 197 - FR
- ["jardiner malin"](https://www.jardiner-malin.fr/fiche/artichaut.html) - FR
- ["saisons-fruits-legumes"](https://www.saisons-fruits-legumes.fr/legumes/artichaut) - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
