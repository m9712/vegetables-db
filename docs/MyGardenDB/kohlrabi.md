[//]: # (---)

[//]: # (tags:)

[//]: # (- ⚠ Need review ⚠)

[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/kohlrabi.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/kohlrabi_6025922?term=kohlrabi&page=1&position=14&page=1&position=14&related_id=6025922&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "kohlrabi",
  "name": "Kohlrabi",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "Another special cabbage, in which neither the inflorescences nor the leaves are consumed, but the stem! In fact, what is called \"rabe\" is in fact the stem that has become hypertrophied. ",
  "preservation": "1 week in a cool place",
  "nutritional": null,
  "vitamin": "Rich in vitamin C",
  "history": "The first European written record is by the botanist Mattioli in 1554 who wrote that it had “come lately into Italy”. By the end of the 16th century, kohlrabi spread to North Europe and was being grown in Austria, Germany, England, Italy, Spain, Tripoli and parts of the eastern Mediterranean.",
  "imagePath": "../../../assets/vegetables/icons/kohlrabi.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 3,
      "coldResistance": 1,
      "rowSpacing": 50,
      "spacingBtRows": 70
    },
    "monthOfPlanting": [4, 5],
    "monthOfHarvest": [6, 8],
    "plantingInterval": null,
    "plantation": "Cabbages are demanding of compost and water, and can adapt to any climate. During cultivation, the larger varieties can be planted with other vegetables, such as lettuce. You can make your cabbage nurseries in the ground. When transplanting, bury the base of the stem to anchor the plants, especially for the cabbage varieties that will have to carry a significant weight. Transplant when the plants have 6 well-developed leaves, from April to May for summer and fall cabbages and in July for winter cabbages.",
    "harvest": "From June to August for summer cabbages and from October to January for winter cabbages.",
    "soil": "Fertile and cool",
    "tasks": "Prevent flea beetle attacks by combining it with aromatic plants",
    "favourableAssociation": ["Spinach", "Lettuce"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Courgette", "Potato"],
    "unfavourablePrecedents": ["Carrot", "Celery", "Bean", "Corn", "Melon", "Tomato"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-feuilles/blette) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 207 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 104
- [Wikipedia](https://en.wikipedia.org/wiki/Kohlrabi)

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
