[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/sorrel.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: icongeek26](https://www.flaticon.com/authors/icongeek26)
- [image info](https://www.flaticon.com/free-icon/sorrel_5888702?term=sorrel&page=1&position=3&page=1&position=3&related_id=5888702&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "sorrel",
  "name": "Sorrel",
  "type": ["plant", null],
  "fromTree": false,
  "family": "Polygonaceae",
  "synopsis": "Almost impertinent, it takes the palate by storm with its fresh, sparkling and tangy taste. Let yourself be surprised!",
  "preservation": "3 days max. in the crisper",
  "nutritional": "23 kcal for 100g",
  "vitamin": "Rich in vitamin C",
  "history": "It is only in the Middle Ages that Man began to cultivate sorrel. It quickly became a staple of French cuisine, being used in the composition of the famous green sauces served at court with big game. Its medicinal and culinary virtues earned it all the honors in the 17th century. It was not until the greedy Catherine de Médicis that it was recognized and cultivated. Even today, many spinach-based dishes are called \"Florentine\".",
  "imagePath": "../../../assets/vegetables/icons/sorrel.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": null,
      "waterNeeds": null,
      "coldResistance": null,
      "rowSpacing": null,
      "spacingBtRows": null
    },
    "monthOfPlanting": 4,
    "monthOfHarvest": 10,
    "plantingInterval": 2,
    "plantation": "In April, plant the splinters 15-20 cm apart.",
    "harvest": "As sorrel is perennial, you can enjoy it all year long. Using a knife, cut off the leaves in handfuls above the collar while the vegetation is active. Harvest leaf by leaf when vegetation has slowed.",
    "soil": "Good soil enriched with potting soil",
    "tasks": "Water to facilitate recovery. Sorrel will be more acidic if you place it in the sun. Hoe",
    "favourableAssociation": ["Spinach", "Parsley", "Radish"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/aromatiques-fraiches/oseille) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 118

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
