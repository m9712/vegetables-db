[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/pea.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/pois_1591275?related_id=1591275&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "pea",
  "name": "Pea",
  "type": ["vegetable", "seed"],
  "fromTree": false,
  "family": "Fabaceae",
  "synopsis": "Its spring-like flavour is a treat in simple, traditional preparations. It is also called snow pea or princess. A vegetable for pleasure, it is the ally of your well-being thanks to its atypical nutritional qualities.",
  "preservation": "3 days in the crisper",
  "nutritional": "80 kcal for 100g",
  "vitamin": "Rich in fibre and vitamin B9",
  "history": "The pea, also called garden pea, is one of the oldest vegetables cultivated in Europe and Asia. In Iran, Palestine, Greece and Switzerland, the pea was already present 10 000 years ago. At that time, it was eaten dry, crushed and then cooked. Its fresh consumption is relatively recent. It was introduced in France in the 17th century via Italy and the Netherlands. It developed quickly around Paris. Louis XIV loved the pea, so he asked his gardener, La Quintinie, to grow it in greenhouses at Versailles. The Austrian monk Mendel used the pea to establish the first laws of genetics. For 18 years, he carried out crossbreeding in his vegetable garden and noted the variations in size, shape and color. If a few dozen varieties were listed in the 16th century, there were already 360 names in 1925. This number has not stopped growing.",
  "imagePath": "../../../assets/vegetables/icons/pea.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 0,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 3,
      "spacingBtRows": 45
    },
    "monthOfPlanting": [2, 3],
    "monthOfHarvest": [5,6],
    "plantingInterval": 4,
    "plantation": "Peas are sown in the fall, in southern regions or by the sea, in early spring, at the end of February or beginning of March. If the spring is dry, watering is necessary at the time of flowering and pod formation, but avoid overwatering because the pea roots are very sensitive to asphyxiation.",
    "harvest": "Proceed by successive pickings, several times a week. Be careful not to uproot the peas, harvest them with one hand while holding the stems with the other. Pick the peas growing at the bottom of the plant first.",
    "soil": "Light and cool",
    "tasks": "Water regularly. Stub the base of the plants when they reach 8 to 10 cm high.",
    "favourableAssociation": ["Carrot", "Lettuce", "Cabbage", "Radish", "Spinach", "Celery"],
    "unfavourableAssociation": ["Onion", "Tomato", "Leek", "Bean"],
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/haricots-pois-legumes-secs-graines-germees/petit-pois) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 260 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 119

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
