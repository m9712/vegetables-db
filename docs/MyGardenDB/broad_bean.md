[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/broad bean.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/premium-icon/jelly-beans_1047909?related_id=1047909&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "broad_bean",
  "name": "Broad bean",
  "type": ["vegetable", "seed"],
  "fromTree": false,
  "family": "Fabaceae",
  "synopsis": "The bean is a legume: it enriches the soil with nitrogen. In the South of France, it is an interesting plant because it grows slowly during winter (it resists up to -4°C), it does not require any care and is one of the first vegetables to be harvested in spring, from the end of April.",
  "preservation": "Seeds can be stored in a dry place and protected from light for 1 year.",
  "nutritional": "82 kcal for 100g",
  "vitamin": "Rich in vitamin B9",
  "history": "It is assumed that the bean originated in the Middle East, more precisely in the south of the Caspian Sea. It was then quickly introduced in Europe, Africa and India. Very popular during Antiquity and the Middle Ages, it gradually but surely gave way to beans and potatoes. It is only from the 15th and 16th century that the bean is consumed fresh. At that time, it was cultivated along rivers, which earned it its name of \"marsh bean\".",
  "imagePath": "../../../assets/vegetables/icons/broad_bean.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 0,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 10,
      "spacingBtRows": 40
    },
    "monthOfPlanting": 3,
    "monthOfHarvest": 6,
    "plantingInterval": null,
    "plantation": "Sow in patches in March (1 seed every 20 cm, buried 3 cm or 4 cm deep).",
    "harvest": "Harvest the first pods to eat the raw seeds as soon as they are thick enough (by squeezing them gently, you can feel the seeds already well formed inside). For a dry harvest, wait until the pod is completely dry. It will then take on a brown or black color.",
    "soil": "Not too dry, likes heavy soil.",
    "tasks": "Hoe once or twice. Pinch the top of each stem at flowering time (May) to accelerate the formation of lower pods and prevent aphid invasion.",
    "favourableAssociation": ["Borage", "Broccoli", "Carrot", "Celery", "Cabbage", "Cauliflower", "Cucumber", "Gherkin", "Courgette", "Spinach", "Strawberry", "Lettuce", "Corn", "Melon", "Turnip", "Carnation", "Radish", "Tomato"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Tomato"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-feuilles/feve) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 233 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 60 and 114

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
