---
tags:
- ⚠ Need review ⚠
---

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/cauliflower.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](<image-url>)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "cauliflower",
  "name": "Cauliflower",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "Raw or cooked, it lends itself to many recipes and has serious nutritional qualities. You can therefore eat it without counting on it!",
  "preservation": "2 to 3 days in the crisper",
  "nutritional": "30 kcal for 100g",
  "vitamin": "Rich in vitamin C and B9",
  "history": "Cauliflower probably originated in the Near East. The Greeks and Romans knew and appreciated it since the Antiquity. But it was then forgotten for a long time. In Europe, cauliflower was only rediscovered in the 16th century, thanks to Italian sailors who brought it back from the Levant... But in France, it became especially trendy in the 17th century, thanks to the gardener of Louis XIV, La Quintinie. He imported his seeds from Cyprus.",
  "imagePath": "../../../assets/vegetables/icons/cauliflower.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 3,
      "coldResistance": 1,
      "rowSpacing": 50,
      "spacingBtRows": 70
    },
    "monthOfPlanting": 5,
    "monthOfHarvest": [9,10],
    "plantingInterval": null,
    "plantation": "Transplant early varieties preferably in May.",
    "harvest": "September-October",
    "soil": "Fertile and fresh",
    "tasks": "Cauliflower is quite demanding. Water and hoe frequently.",
    "favourableAssociation": ["Celery", "Tomato"],
    "unfavourableAssociation": ["Cabbage", "Onion", "Potato"],
    "favourablePrecedents": ["Broad bean", "Lettuce"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information
No link for image info !

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/choux/chou-fleur) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 59 and 108

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
