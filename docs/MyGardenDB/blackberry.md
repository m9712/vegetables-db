﻿[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (LINKS :)
[//]: # (- Documentation: http://127.0.0.1:8000/Getting%20started/add-vegetables/)
[//]: # (- Need help ?: https://discord.gg/XPNBsarq8t)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/blackberry.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik ](https://www.freepik.com)
- [image info](https://www.flaticon.com/free-icon/blackberry_5135754?term=blackberry&page=2&position=22&origin=search&related_id=5135754)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "id": "blackberry",
  "name": "Blackberry",
  "type": ["fruit", null],
  "fromTree": true,
  "family": "Rosaceae",
  "synopsis": "Blackberries are succulent, soft, and juicy. Their flavor is sweet, slightly tart, with earthy undertones. Delicious when eaten in jam !",
  "preservation": "2 to 3 days in a fridge",
  "nutritional": "43 kcal for 100g",
  "vitamin": "Rich in vitamin C and K, rich in fiber",
  "history": "Present almost everywhere, this perennial shrub, crawling (and prickly!), is often considered as an invasive plant. The blackberry is invited in the Greek mythology: it would come from the blood of the Titans, poured during their fights against the gods. In the 15th century, the blackberry was eaten as an appetizer, simply as a snack.",
  "imagePath": "../../../assets/vegetables/icons/blackberry.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": null,
      "waterNeeds": null,
      "coldResistance": null,
      "rowSpacing": 2500,
      "spacingBtRows": null
    },
    "monthOfPlanting": [5, 6],
    "monthOfHarvest": [7, 8],
    "plantingInterval": null,
    "plantation": "Pruning is simple since fruits are formed on secondary second-year shoots, which die after fruiting. It is therefore sufficient to eliminate them and let the young branches settle in, thinning them out a little if they are too numerous or stunted.",
    "harvest": "The fruits of the domestic varieties are larger than those of the wild bramble They do not keep well and must therefore be picked as and when needed. The harvest generally lasts more than a month, it is greater than that of the wild bramble, around 10 kg per vine.",
    "soil": null,
    "tasks": null,
    "favourableAssociation": null,
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/mure) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 317 - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **Emilio** - 2023 - ebriand@et.esiea.fr
