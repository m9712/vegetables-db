[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/celery.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/celeri_6969440?term=c%C3%A9leri&page=1&position=50&page=1&position=50&related_id=6969440&origin=tag)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "celery",
  "name": "Celery",
  "type": ["vegetable", "stem"],
  "fromTree": false,
  "family": "Apiaceae",
  "synopsis": "Celery root and celeriac are the same species. They require a lot of compost, especially celeriac, and regular watering (celery was originally a marsh plant...). Celery root can be left in the ground over winter in mild climates.",
  "preservation": "5 days in the crisper",
  "nutritional": "14 kcal for 100g",
  "vitamin": null,
  "history": "Originating from the Italian peninsula, the Greeks used it daily since Antiquity as a medicinal and aromatic plant. Celery sticks were not cooked until the Renaissance, but they were still only a condiment. After a detour through Germany, the vegetable spread to French plates during the 19th century.",
  "imagePath": "../../../assets/vegetables/icons/celery.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": 1,
      "rowSpacing": 30,
      "spacingBtRows": 40
    },
    "monthOfPlanting": [5,6],
    "monthOfHarvest": 8,
    "plantingInterval": null,
    "plantation": "Sowing is delicate because the seeds are tiny and the seedlings, which grow very slowly, are fragile. It is therefore preferable to sow them in pots indoors. Count about 2 months before planting them in the ground, between mid-May and end of June. Mulch copiously and water regularly, avoiding wetting the leaves to limit the development of cryptogamic diseases.",
    "harvest": "End of August until frost. You can harvest the whole plant or just a few ribs as needed.",
    "soil": "Rich & Furnished",
    "tasks": "When planting, push the collar in 2 cm deep. Keep the soil cool and mulch if necessary.",
    "favourableAssociation": ["Cabbage", "Spinach", "Bean", "Leek", "Tomato"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Watercress", "Shallot", "Broad bean", "Turnip", "Potato"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-feuilles/celeri-branche) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 211 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 106

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
