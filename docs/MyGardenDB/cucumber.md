[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (1. Replace <vegetablename> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/cucumber.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-premium/concombre_4278474?term=concombre&page=1&position=27&page=1&position=27&related_id=4278474&origin=search)

### JSON

[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  #(Please don't remove this line)
```json linenums="1"
{
  "id": "cucumber",
  "name": "Cucumber",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Cucurbitaceae",
  "synopsis": "Every year, it is a great success. In France, we consume about 1.8 kilos per year and per person. Raw or cooked, learn to vary the pleasures!",
  "preservation": "7 days max. in the refrigerated bin",
  "nutritional": "14 kcal per 100g",
  "vitamin": "Riche en potassium",
  "history": "Born in all likelihood in northern India (where it is the subject of many legendary stories), the cucumber spread very early to China and the Middle East. It was then cultivated on the banks of the Nile by the Egyptians, who consumed it a lot and gave it as offerings to their gods. The Hebrews also brought it to the Promised Land, where it became one of their favorite foods. During the Antiquity, Greeks and Romans also appreciated cucumbers a lot, in spite of its strong bitterness. Pliny even reports that the emperor Tiberius used to eat it daily, and that gardeners used to grow it under a bell to accelerate its growth. In France, we find official mention of its presence since the 9th century, when Charlemagne ordered the cultivation of it in his domains. In the 17th century, Louis XIV is also very fond of it. In order to serve it to him as a \"primeur\", La Quintinie, the chief gardener of Versailles, will develop the production under greenhouse. Today, the cucumber is mainly cultivated in this way.",
  "imagePath": "../../../assets/vegetables/icons/cucumber.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": 60,
      "spacingBtRows": 100
    },
    "monthOfPlanting": 6,
    "monthOfHarvest": 8,
    "plantingInterval": null,
    "plantation": "Do not plant it in the ground until late April or early May, like tomatoes. To save space, but also so that its foliage is more airy (and thus less attacked by powdery mildew), stake your plants because, like many cucurbits, the cucumber has tendrils that allow it to pull itself up on stakes or other plants present around it.",
    "harvest": "From mid-August (3 months after planting)",
    "soil": "Light, rich in loose humus",
    "tasks": "Water the cucumber plants regularly but do not water the foliage. Mulch around the plants to prevent weeds and limit water evaporation.",
    "favourableAssociation": ["Aubergine", "Tomato"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Corn"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)

- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-fruits/concombre) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 222 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 123


### Contributors

[//]: # (If you modify the file you can put your name at the top of the list)

* **Adrien DERACHE** - 2022 - a.d44@tuta.io
