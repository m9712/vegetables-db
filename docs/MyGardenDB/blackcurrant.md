[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/blackcurrant.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "blackcurrant",
  "name": "Blackcurrant",
  "type": ["fruit", null],
  "fromTree": true,
  "family": "Grossulariaceae",
  "synopsis": "Two large varieties make up the majority of the crops, concentrated in four French regions. Its juicy, acidic fruit can be eaten plain or prepared as a dessert or in the form of liqueur and cream.",
  "preservation": "24 to 48 hours in the crisper",
  "nutritional": "71 kcal for 100g",
  "vitamin": "Rich in vitamin C",
  "history": "Between the 18th and 19th century, blackcurrant was surrounded by medicinal virtues, reputed to be able to overcome all sorts of ills: fevers, parasites, migraines... Its miraculous reputation is attenuated with the profit of its culture, which extends more and more since the middle of the XIXth century in the wine area of Burgundy, then to all the rest of France. The Côte d'Or remains the stronghold of the small dark berry, from which a liqueur and a cream famous all over the world are made.",
  "imagePath": "../../../assets/vegetables/icons/blackcurrant.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": null,
      "waterNeeds": null,
      "coldResistance": null,
      "rowSpacing": 150,
      "spacingBtRows": 150
    },
    "monthOfPlanting": [3, 4],
    "monthOfHarvest": [6, 7],
    "plantingInterval": null,
    "plantation": "Mulching is particularly important at the foot of the blackcurrant tree, as its roots are quite shallow. It will keep them cool, while limiting the growth of weeds.",
    "harvest": "Production can start 2 years after planting, and one plant can yield up to 4 kg of fruit, even more in full production. The berries are not quite ripe with the change of color from red to black: taste them and harvest them when they become quite sweet.",
    "soil": "Light, rich in humus",
    "tasks": null,
    "favourableAssociation": null,
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/petits-fruits-et-fruits-rouges/cassis) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 287 - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
