[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/beetroot.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/beetroot_676403?term=beetroot&page=1&position=33&page=1&position=33&related_id=676403&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "beetroot",
  "name": "Beetroot",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Chenopodiaceae",
  "synopsis": "Its very soft and sweet taste, delicate, is appreciated in crudenesses or cooked and accommodated.",
  "preservation": "In the crisper",
  "nutritional": "423 kcal for 100g",
  "vitamin": null,
  "history": "Both were born from the evolution of the maritime chard, which grew wild on the shores of the Mediterranean and the Atlantic.",
  "imagePath": "../../../assets/vegetables/icons/beetroot.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 15,
      "spacingBtRows": 30
    },
    "monthOfPlanting": [4, 5],
    "monthOfHarvest": 7,
    "plantingInterval": null,
    "plantation": "Transplant in April-May (plants should have 4 to 5 leaves).",
    "harvest": "From July",
    "soil": "Light and rich in humus.",
    "tasks": "The beet requires light and water but not too much. Hoe carefully around the plants and mulch to preserve moisture.",
    "favourableAssociation": ["Dill", "Bean", "Onion", "Savory"],
    "unfavourableAssociation": ["Spinach"],
    "favourablePrecedents": ["Garlic", "Lettuce", "Pea"],
    "unfavourablePrecedents": ["Carrot", "Spinach"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/betterave) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 205 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 104

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
