[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (LINKS :)
[//]: # (- Documentation: https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/)
[//]: # (- Need help ?: https://discord.gg/XPNBsarq8t)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/shallot.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Umeicon](https://www.flaticon.com/authors/umeicon)
- [image info](https://www.flaticon.com/free-icon/shallot_6490227?term=shallot&page=1&position=10&origin=search&related_id=6490227)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "shallot",
  "name": "Shallot",
  "type": ["vegetable", "bulb"],
  "fromTree": false,
  "family": "Liliaceae",
  "synopsis": "It is grown mainly in western France and Brittany. Available all year round, shallots are a welcome addition to your daily recipes.",
  "preservation": "1 or 2 months in a dry, cool, shaded place 2 to 3 days in the crisper",
  "nutritional": "66 kcal per 100g",
  "vitamin": null,
  "history": "It was in Turkestan, a region of Asia bordering the Caspian Sea, that the first variety of shallot saw the light of day. It takes its name from the town of Ascalon, in Judea. The Persians and Egyptians soon adopted the little vegetable as a sacred plant.",
  "imagePath": "../../../assets/vegetables/icons/shallot.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 2,
      "rowSpacing": 10,
      "spacingBtRows": 30
    },
    "monthOfPlanting": [2, 3],
    "monthOfHarvest": 7,
    "plantingInterval": 6,
    "plantation": "Planted in autumn, they require no watering, except if spring is dry, to ensure that bulbs finish growing from the 6-7 leaf stage. All watering must be stopped 15 days before harvest for conversation varieties.",
    "harvest": "From July.",
    "soil": "Loose and not too damp.",
    "tasks": "Clean the soil and weed without damaging the bulbs. Clear the soil around the feet in case of excess humidity.",
    "favourableAssociation": ["Carrot", "Strawberry", "Lettuce"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": ["Potato"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/echalote) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 250 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 112

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2023 - a.d44@tuta.io
