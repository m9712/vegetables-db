[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (LINKS :)
[//]: # (- Documentation: https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/)
[//]: # (- Need help ?: https://discord.gg/XPNBsarq8t)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/melon.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info]()

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "melon",
  "name": "Melon",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Cucurbitaceae",
  "synopsis": "Round and tasty, it's one of France's favorite summer fruits and vegetables. Today, it is eaten mainly as a fruit, but can also be used to enhance savoury dishes.",
  "preservation": "6 days max. in crisper",
  "nutritional": "62 kcal per 100g",
  "vitamin": "Rich in vitamin B9",
  "history": "The melon is thought to have originated in Africa. The Egyptians cultivated it as early as 500 BC. It reached Greece and then Rome around the 1st century, where it was consumed as a vegetable. At the time, it was small and not very sweet, and was eaten with pepper and vinegar. Its flavor and aroma gradually became more refined.",
  "imagePath": "../../../assets/vegetables/icons/melon.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": 75,
      "spacingBtRows": 1
    },
    "monthOfPlanting": [4, 5],
    "monthOfHarvest": 7,
    "plantingInterval": 4,
    "plantation": "Sow in April-May for planting in late May or early June. Fruit formation takes place on row 3 branches. We therefore recommend pruning to speed up fruiting: cut the main stem above the second leaf, pinch off the two growing stems of row 2 above the fourth leaf, to obtain four shoots of row 3, on which the fruit will develop. Keep only 2 or 3 per plant, and cut off the stem two leaves after each fruit to encourage enlargement. Watering should be regular, but for the sugars to form properly, it should stop when the fruits have reached their final size.",
    "harvest": "Melons should be harvested when ripe, i.e. when their fragrance is well developed (bend down to smell them!), the leaf extending from the stalk is wilting, and the point of attachment of the stalk to the fruit is cracked, causing a clean break when a very slight twist is applied by turning the fruit on itself. Melon can be kept for a few days after picking, but is best eaten when still firm.",
    "soil": null,
    "tasks": "Like other cucurbits, melons are susceptible to a number of fungal diseases, notably powdery mildew. Respect a period of 4 years between two melon crops in the same location. It also fears snails and slugs, but only for a few days after planting.",
    "favourableAssociation": ["Corn"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Spinach"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-fruits/melon) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 246 - FR


### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2023 - a.d44@tuta.io
