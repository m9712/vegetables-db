[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/pear.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/free-icon/pear_15521464.htm#page=4&query=pear%20vegetarian&position=9&from_view=keyword)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "pear",
  "name": "Pear",
  "type": ["fruit", null],
  "fromTree": true,
  "family": "Rosaceae",
  "synopsis": "Enjoy it as a snack or reveal its sweetness in many desserts or savory recipes. Adopt it, it only wants to do you good!",
  "preservation": "3 to 7 days at room temperature",
  "nutritional": "53 kcal pour 100g",
  "vitamin": "Rich in vitamin B9",
  "history": "Little by little, Europe got to know it. In France, it appeared in the 16th century and King Louis XIV popularized it thanks to his gardener La Quintinie. Several species of pear trees were cultivated in the famous 'king's garden'. Over the centuries, multiple varieties were selected and retained. In France, about ten are currently produced. One of the most popular is the Conference, born in England. It owes its name to the prize it won at the British National Pear Conference in 1885.",
  "imagePath": "../../../assets/vegetables/icons/pear.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": null,
      "waterNeeds": null,
      "coldResistance": null,
      "rowSpacing": 700,
      "spacingBtRows": 700
    },
    "monthOfPlanting": [3, 4],
    "monthOfHarvest": [7, 11],
    "plantingInterval": null,
    "plantation": "If the pear tree likes deep and fresh soils, it is however necessary to avoid wet areas, favourable to the development of cryptogamic diseases. In order to obtain beautiful fruits, keep only one pear per bunch.",
    "harvest": "Summer pears are picked a little before they are fully ripe and eaten about a week later. Fall pears are harvested when fully ripe.",
    "soil": "Fresh and deep",
    "tasks": "Pear trees prefer humidity. Be sure to water the first year and in dry weather",
    "favourableAssociation": null,
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/fruits-a-pepins/poire) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 311 - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
